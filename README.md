# Sphinx documentation of a child class won't show when "autodoc_mock_imports" is enabled

## Question

Is there a way to generate sphinx documentation of a child class without
installing the library containing its parent class on GitLab CI
(or any comparable CI tool)?

NOTE: If the problem cannot be solved by Sphinx settings alone, I will
only accept an answer which contains a concrete way to get the desired
documentation generated and published. A ``.gitlab-ci.yml`` file should
be provided in this case.

## Context

Specifically, I made a child class of ``tensorflow.keras.callbacks.Callback``
and want to show its docstring on the documentation page.
By default, Sphinx has to import everything generate documentation. But
it doesn't seem right to install ``tensorflow`` (and tens of other libraries
that sums up to several GBs) on the CI image just for this. This is the reason
why I turned on ``autodoc_mock_imports`` in ``conf.py`` (the Sphinx
configuration file). The docs were build without error, but the documentation
of that child class was missing.

The sphinx directives of the docs is

    .. automodule:: keras_callback

        :members:
        :inherited-members:

## Minimum Working Example

There is an [MWE](https://gitlab.com/cbhuang/test-sphinx-mock)
and [Sphinx-generated docs](https://cbhuang.gitlab.io/test-sphinx-mock/keras_callback.html).

Desired documentation of the child class:
![](local.png)
