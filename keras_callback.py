"""
This module contains an inherited class and an independent function.
With ``autodoc_mock_imports = ['tensorflow']`` enabled in ``conf.py``,
the inherited class won't show.
"""

from tensorflow.keras.callbacks import Callback


class MyKerasCallback(Callback):
    """An interited Keras ``Callback`` class"""

    def __init__(self, dic=None):
        super().__init__()

    def on_epoch_begin(self, epoch, logs=None):
        """Inherited method"""
        print("This is an inherited method.")

    def custom_method(self):
        """Custom method"""
        print("This is a custom method.")


def util_func():
    """Some utility function"""
    print("This is an independent utility function.")
